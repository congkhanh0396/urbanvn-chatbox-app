import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
// to use navigion version 5 , import this, and wrap the rest of code by navigationContainer
import { createStackNavigator } from '@react-navigation/stack';
import ChatAppNavigator from './navigation/ChatAppNavigator';

const Stack = createStackNavigator();

export default function App() {
  return (
    // to use navigion version 5 , import this, and wrap the rest of code by navigationContainer
    <NavigationContainer>
      <ChatAppNavigator/>
    </NavigationContainer>
  );
}

