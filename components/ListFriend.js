import React from 'react';
import { View, Text, Image } from 'react-native';
const ListFriend = (props) => {
    return (
        <View style={{ marginHorizontal: 5, marginVertical: 5, flexDirection: 'row' }}>
            <View style={{ marginVertical: 10, marginHorizontal: 10 }}>
                <Image style={{
                    width: 50,
                    height: 50,
                    borderRadius: 50/2
                }} source={{
                    uri: 'https://reactnative.dev/img/tiny_logo.png',
                }} />
            </View>
            <View style={{ flex: 1, justifyContent: 'space-evenly', paddingHorizontal: 10 }}>
                <Text>{props.name}</Text>
                <Text style={{ color: '#c5c5c5' }}>{props.message}</Text>
            </View>
            <View style={{ alignItems: 'flex-end', justifyContent: 'space-evenly' }}>
                <Text style={{ fontSize: 15, paddingHorizontal: 5 }}>{props.time}</Text>
                <Text style={{ color: 'black', alignSelf: 'center'}}>{props.count}</Text>
            </View>
        </View>
    )
}

export default ListFriend;