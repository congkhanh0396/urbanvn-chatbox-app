import React from 'react';
import { View, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
const SearchBox = () => {
    return (
        <View style={{ backgroundColor: '#d2d4d2', marginHorizontal: 5, marginVertical: 10, flexDirection: 'row', borderRadius: 15 }}>
            <Icon name='search-outline' size={20} color='#adacac' style={{ alignSelf: 'center', padding: 15 }} />
            <TextInput placeholder="Search message ..." />
        </View>
    )
}

export default SearchBox;