import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../screens/LoginScreen';
import DetailScreen from '../screens/DetailScreen';
import ChatScreen from '../screens/ChatScreen';
import Icon from 'react-native-vector-icons/Ionicons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';


const LoginStack = createStackNavigator();

const LoginStackScreen = ({ navigation }) => (
    <LoginStack.Navigator screenOptions={{
        headerStyle: { backgroundColor: '#01736e' },
        headerTintColor: '#fff',
        headerTitleAlign: 'center',
    }}>
        <LoginStack.Screen name='login' component={LoginScreen} options={{
            title: 'Chat Box',
            headerTitleStyle: {
                fontSize: 40
            },
            headerStyle: {
                backgroundColor: '#01736e',
                height: 180,
            }
        }}
        />
    </LoginStack.Navigator>
)


const ChatStack = createStackNavigator();

const ChatStackScreen = () => (
    <LoginStack.Navigator screenOptions={{
        headerStyle: { backgroundColor: '#01736e' },
        headerTintColor: '#fff',
        headerTitleAlign: 'center',
    }}>
        <LoginStack.Screen name='chat' component={ChatScreen} options={{ headerRight: () => <Icon size={30} style={{ paddingRight: 10 }} color='#fff' name='add-outline' /> }} />
    </LoginStack.Navigator>
)

const Tab = createBottomTabNavigator();

function ChatAppNavigator() {
    return (
        <Tab.Navigator tabBarOptions={{ tabStyle: { backgroundColor: '#01736e' }, activeTintColor: 'white' }}>
            {/* hide tabBar in loginScreen */}
            <Tab.Screen name="login" component={LoginStackScreen} 
            options={({ route }) => ({
                tabBarVisible: false
            })}
             />
            <Tab.Screen name="Chat" component={ChatStackScreen} />
            <Tab.Screen name="Contact" component={ChatStackScreen} />
            <Tab.Screen name="Settings" component={ChatStackScreen} />
        </Tab.Navigator>

    )
}



export default ChatAppNavigator;
