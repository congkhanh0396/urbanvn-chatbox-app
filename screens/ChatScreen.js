import React from 'react';
import { View, StyleSheet } from 'react-native';
import SearchBox from '../components/SearchBox';
import ListFriend from '../components/ListFriend';
import { TouchableOpacity } from 'react-native-gesture-handler';
const ChatScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <SearchBox />
            <ListFriend name='Yosuke Higuchi' message='i belong to Urban Corporation ...' time='12:10' count={10} />
            <ListFriend name='Shinpei Sata' message='(こんにちは！)' time='2021/02/05' />
            <ListFriend name='Công Khanh' message='(お疲れ様でした！！！)' time='2021/02/04' />
            <ListFriend name='Lâm Dương' message='(お疲れ様でした！！！)' time='22:08' />
            <ListFriend name='Vĩnh Huy' message='(お疲れ様でした！！！)' time='9:10' />
        </View>
    )
}

export default ChatScreen;
const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});