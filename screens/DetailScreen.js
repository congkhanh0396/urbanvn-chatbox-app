import React from 'react';
import {View, Text, Button} from 'react-native';

const DetailScreen = ({navigation}) => {
    return (
        <View>
            <Text>This is detail Screen</Text>
            <Button title='Go to login screeen' onPress={() => navigation.pop()} />
        </View>
    )
}

export default DetailScreen;