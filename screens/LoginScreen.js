import React from 'react';
import { View, Text, TextInput, TouchableWithoutFeedback, StyleSheet, Keyboard } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity } from 'react-native-gesture-handler';
const LoginScreen = ({ navigation }) => {

    return (
        <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss(); }}>
            <View style={styles.loginHeader}>
                <View style={styles.loginContainer}>
                    <View style={styles.loginWrapper}>
                        <View style={styles.bioUser}>
                            <Icon style={styles.loginIcon} name="person" size={20} color="#017473" />
                            <TextInput placeholder='Full fill the blank with usename' style={{ flex: 1 }} keyboardType="visible-password" maxLength={25} />
                        </View>
                    </View>
                    <View style={styles.loginWrapper}>
                        <View style={styles.bioUser}>
                            <Icon style={styles.loginIcon} name="lock-closed" size={20} color="#01736e" />
                            <TextInput placeholder='Full fill the blank with password' style={{ flex: 1 }} secureTextEntry={true} />
                        </View>
                    </View>
                    <View style={{ marginTop: '20%' }}>
                        <Text style={{ color: '#01736e', fontSize: 12, fontWeight: 'bold', textAlign: 'right' }}>Forgot your password ?</Text>
                    </View>
                    <View style={{ marginTop: '12%' }}>
                        <TouchableOpacity style={styles.btnLogin} onPress={() => navigation.navigate('Chat')}>
                            <Text style={styles.btnStyle}>Sign in</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginTop: '15%' }}>
                        <Text style={{color: '#01736e',fontSize: 12,fontWeight: 'bold',textAlign: 'center'}}>Create account</Text>
                    </View>
                </View>
            </View>
        </TouchableWithoutFeedback>
    )
}

export default LoginScreen;

const styles = StyleSheet.create({

    loginHeader: {
        flexDirection: 'column',
        justifyContent: 'center',
        paddingTop: 80,
        paddingBottom: 80,
        paddingLeft: 0,
        paddingRight: 0
    },
    loginWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10,
        marginTop: 20
    },
    loginContainer: {
        height: '100%',
        marginRight: '10%',
        marginLeft: '10%'
    },
    bioUser: {
        flexDirection: 'row',
        borderRightColor: 'black',
        borderBottomWidth: 1,
        paddingBottom: 5,
        width: '100%'
    },
    loginIcon: {
        alignSelf: 'center',
        marginRight: 8
    },
    btnLogin: {
        elevation: 8,
        backgroundColor: "#017473",
        borderRadius: 20,
        paddingVertical: 12,
        marginHorizontal: 20
    },
    btnStyle: {
        color: '#fff',
        fontSize: 15,
        fontWeight: 'bold',
        textAlign: 'center'
    }
});
